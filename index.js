const express = require("express");
const app = express();
const open = require("open");
var http = require("http").createServer(app);
var io = require("socket.io")(http);
const { firmarDocumento } = require("./utiles/utiles");
const { getAlias } = require("./controllers/controllers");
const axios = require("axios");
const fs = require("fs");

const esquemaFirmante = require("./model/firmante");

const PATH_FIRMADOR = "./signedApp/JSignPdf.jar";
const out = "./archivos/firmas/";

let estado = {
  firmantes: [],
  isActive: false,
  res: null,
  usuarioActual: { nombre: "" },
};
/*
 * @param {object} newState
 */
/**
 * @param {object} newState
 *
 */
const setState = (newState) => {
  for (let key in newState) {
    estado[key] = newState[key];
  }
  return estado;
};

const getState = () => estado;

app.use(express.static("public"));

app.get("/firmar", async (req, res) => {
  try {
    if (!estado.isActive) {
      let firmantes = JSON.parse(req.query.firmantes);
      let nombreRandom =
        Math.floor(Math.random() * Math.floor(1000000)) + ".pdf";
      const urlPdf = req.query.archivo;

      if (!firmantes) {
        throw { error: "Envie firmantes por favor", status: 400 };
      }

      let img = await axios({
        url: urlPdf,
        method: "GET",
        responseType: "stream",
      });

      const pathEntrada = `./archivos/temp/${nombreRandom}`;
      let writable = img.data.pipe(fs.createWriteStream(pathEntrada));
      await open("http://localhost:3000/");

      writable.on("finish", () => {
        estado.usuarioActual = {
          ...firmantes.shift(),
          entrada: pathEntrada,
          salida: out,
        };
        estado.res = res;
        estado.firmantes = firmantes;

        estado.isActive = true;
        console.log(estado.usuarioActual);
        io.emit("isActive", {
          isActive: estado.isActive,
          nombre: estado.usuarioActual.nombre,
        });

        setTimeout(() => {
          if (estado.res) {
            estado.res.status(500).send({ error: "time out" });
          }
          estado = {
            firmantes: [],
            isActive: false,
            res: null,
            usuarioActual: { nombre: "" },
          };
          io.emit("isActive", { isActive: estado.isActive, nombre: "" });
        }, 10000);
      });
    } else {
      res.status(500).json({ error: "El servicio es en uso" });
    }
  } catch (error) {
    console.log(error);
    estado = {
      firmantes: [],
      isActive: false,
      res: null,
      usuarioActual: { nombre: "" },
    };
    io.emit("isActive", { isActive: estado.isActive, nombre: "" });
    res.status(error.status || 500).json(error);
  }
});

io.on("connection", (socket) => {
  socket.on("finalizar", () => {
    if (estado.res) {
      estado.res.json({});
    }
    estado = {
      firmantes: [],
      isActive: false,
      res: null,
      usuarioActual: { nombre: "" },
    };
    io.emit("isActive", { isActive: estado.isActive, nombre: "" });
  });

  socket.emit("isActive", {
    isActive: estado.isActive,
    nombre: estado.usuarioActual.nombre,
  });

  socket.on("firmar", (param) => {
    //  firmar
    try {
      let respuesta = firmarDocumento(
        estado.usuarioActual.alias,
        PATH_FIRMADOR,
        estado.usuarioActual.entrada,
        estado.usuarioActual.salida
      );

      if (estado.firmantes.length <= 0) {
        estado.res.json({
          mensaje: "finalizado con exito",
          respuesta: respuesta.toString(),
        });
        estado = {
          firmantes: [],
          isActive: false,
          res: null,
          usuarioActual: { nombre: "" },
        };
        socket.emit("isActive", {
          isActive: estado.isActive,
          nombre: estado.usuarioActual.nombre,
        });

        return;
      }
      estado.usuarioActual = estado.firmantes.shift();
      socket.emit("editText", { nombre: estado.usuarioActual });
    } catch (error) {
      if (estado.res) {
        estado.res.status(500).send({ error });
      }
      estado = {
        firmantes: [],
        isActive: false,
        res: null,
        usuarioActual: { nombre: "" },
      };
      io.emit("isActive", { isActive: estado.isActive, nombre: "" });
    }
    console.log(firmantes);
  });
});

app.get("/obtenerAlias", getAlias);

http.listen(3000);
