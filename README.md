#  Instalacion

### 1.0 Node js 
La aplicacion requiere [Node.js](https://nodejs.org/)

Instalar dependencias y ejecutar el servidor
```sh
$ cd firmaDigital
$ npm install
$ node index.js
```

### 1.1 Dependencias

En la cartpa signedApp esta la libreria pricipal [JSignedPdf](http://jsignpdf.sourceforge.net/)
Esta es la libreria principal que se encarga de firmar documentos, El desarrollo en nodejs solo se va a usar para editar el pdf, brindar servicio web o cualquier otra cosa que sea necesaria usar como proxy.

### 1.2 Java
Es necesario tenes en el cliente ademas la maquina virtual de java


## 2) Servicios 
La aplicacion tiene 2 rutas
```
/obtenerAlias
/firmar
```

### obtenerAlias
get Service
Esta ruta obtiene todos los alias que esten almacenado en los certificado propios en windows. en un arreglo


#### Ejemplo
http://localhost:3000/obtenerAlias

### Firmar
get Service
Esta ruta firma un documento determinado por el cliente los parametros que recibe son
```
firmantes=[{"alias":"unAliasDeWindowsMy","nombre":"Un nombre"}]
archivo="http://pathtoImagen.png"
```
#### ejemplo
http://localhost:3000/firmar?firmantes=[{"alias":"joan","nombre":"joan"}]&archivo=http://ww4.neuquen.gov.ar/pecas/produccion/tareafirmadigital/pdf/boletomarca_146.pdf