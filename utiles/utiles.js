const { execSync } = require("child_process");

/**
 * @param {string} alias
 * @param {string} pathFirmador
 * @param {string} inputPdf
 * @param {string} outPdf
 */
const firmarDocumento = (alias,pathFirmador,inputPdf,outPdf) =>{
    try {
        let command = `java -jar ${pathFirmador} -kst WINDOWS-MY ${inputPdf} -ka ${alias} -d ${outPdf}`;
        console.log(command)
        let respuesta = execSync(command);
        return respuesta;
    }catch(error){
        console.log(error.stdout.toString())
        throw {error:error.stdout.toString()}
    }
}

module.exports = {
    firmarDocumento
}
