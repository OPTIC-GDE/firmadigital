const Schema =  require('validate')


const firmante = new Schema({
    nombre:{
        type: String,
        required:true,
        message: {
            type: 'El nombre tiene que ser del tipo string',
            required: 'El nombre es requerido'
        }
    },
    alias:{
        type:String,
        required:true,
        message: {
            type: 'El alias tiene que ser del tipo string',
            required: 'El alias es requerido'
          }
    },
    imagen:{
        type:String,
        required:true,
        message: {
            type: 'la imagen tiene que ser del tipo string',
            required: 'La imagen es requerida'
          } 
    },
    posX:{
        type:Number,
        required:true,
        message:{
            type: 'posX tiene que ser del tipo numerico',
            required: 'La posX es requerida'
        }
    },
    posY:{
        type:Number,
        required:true,
        message:{
            type: 'posY tiene que ser del tipo numerico',
            required: 'La posY es requerida'
        }
    }

})

module.exports = firmante;